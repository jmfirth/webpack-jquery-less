var config = require('./webpack.config')
var path = require('path')

config.output = {
  path: path.join(__dirname, 'dist'),
  filename: 'bundle.js'
}

module.exports = config
