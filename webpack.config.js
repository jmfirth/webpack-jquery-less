var ExtractTextPlugin = require('extract-text-webpack-plugin')
var path = require('path')

module.exports = {
	entry: {
		app: './src/index.js'
	},
	module: {
		loaders: [{
			test: /\.jsx?$/,
			exclude: /(node_modules|bower_components)/,
			loader: 'babel',
			query: {
				presets: ['es2015']
			}
		}, {
			test: /\.less$/,
			loader: ExtractTextPlugin.extract(
				'css?sourceMap!' +
				'less?sourceMap'
			)
		}, {
			test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
			loader: "url-loader?limit=10000&mimetype=application/font-woff"
		}, {
			test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
			loader: "file-loader"
		}]
	},
	plugins: [
		new ExtractTextPlugin('styles.css')
	]
}
