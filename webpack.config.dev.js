var config = require('./webpack.config')
var path = require('path')

config.output = {
  path: path.join(__dirname, 'dev'),
  filename: 'bundle.js'
}

config.devtool = 'source-map'

module.exports = config
