# webpack-jquery-less

This repository contains an example webpack configuration for a jQuery and LESS-based site.  It has separate dev and prod configurations which are extensions of the base configuration.

This configuration assumes the files are in a `./src` folder.  To modify this assumption, edit `webpack.config.js` and change the path specified in the `entry` section.

## Installing

```sh
$ npm install
```

## Development

To produce and dev build, rebuilding on change:

```sh
$ npm run watch
```

To produce a dev build only:

```sh
$ npm run build:dev
```

In either case, the dev configuration outputs files to `./dev`.

## Distribution

To produce a production build for distribution:

```sh
$ npm run build
```

The prod configuration outputs files to `./dist`.
